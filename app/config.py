DEBUG = True
SECRET_KEY = "DEVELOPMENT KEY"
SQLALCHEMY_DATABASE_URI = "mysql://root@localhost/testdatabase"
WTF_CSRF_ENABLED = False
RBAC_USE_WHITE = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
