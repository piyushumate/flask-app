from flask.ext.login import UserMixin
from flask.ext.rbac import RoleMixin
from .. import db , bcrypt, app
from . import rbac
from .. import db , bcrypt 
from .. import app
from flask.ext.user import SQLAlchemyAdapter, UserManager




class Role(db.Model):
	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(50), unique=True)

class User(db.Model, UserMixin):
	#__tablename__ = 'users'
   
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	email = db.Column(db.String(60), nullable=False, unique=True)
	password_hash = db.Column(db.String(255), nullable=True)

	
	roles = db.relationship(
            'Role',
            secondary='user_roles',
            backref=db.backref('users', lazy='dynamic')
        )
	
	def __init__(self, email, password, roles=[]):
		self.email = email
		self.set_password(password)

	def set_password(self, password):
		self.password_hash = bcrypt.generate_password_hash(password)

	def hash_password(self, password):
		return bcrypt.generate_password_hash(password)

	def check_password(self, password):
		return bcrypt.check_password_hash(self.password_hash, password)

class UserRoles(db.Model):
	id = db.Column(db.Integer(), primary_key=True)
	user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
	role_id = db.Column(db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))
	
db_adapter = SQLAlchemyAdapter(db,  User)
user_manager = UserManager(db_adapter, app)