from flask_wtf import Form
from wtforms import TextField, PasswordField, SubmitField, validators
from models import db, User

class SignUpForm(Form):
	email = TextField('email', [validators.Required("Please enter your email address."), validators.Email("Enter valid email address")])
	password = PasswordField('password', [validators.Required("Please enter a password.")])

	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.user = None

	def validate(self): 
		if not Form.validate(self):
			return False
		user = User.query.filter_by(email = self.email.data.lower()).first()
		if user:
			self.email.errors.append("Email is already registered")
			return False
		else:
			return True

	def get_user(self):
		return User.query.filter_by(email=self.email.data).first()

class LoginForm(Form):
	email = TextField('email',  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
	password = PasswordField('password', [validators.Required()])

	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.user = None

	def validate(self):
		if not Form.validate(self):
			return False
		user = User.query.filter_by(email=self.email.data).first()
		if not user:
			self.email.errors.append('Email not registered')
			return False
		if not user.check_password(self.password.data):
			self.password.errors.append('Invalid password')
			return False

		self.user = user
		return True

	def get_user(self):
		return User.query.filter_by(email=self.email.data).first()