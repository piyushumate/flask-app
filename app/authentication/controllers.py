from flask import Blueprint, jsonify, session, flash, abort, redirect, render_template, request, url_for, g, current_app
from flask.ext.login import login_required, login_user, logout_user, current_user
from flask.ext.user import roles_required
from models import User, Role
from forms import SignUpForm,LoginForm
from .. import db
from . import login_manager 


authentication = Blueprint('authentication', __name__, template_folder='templates')


@login_manager.user_loader
def load_user(user_id):
	return User.query.get(user_id)

@authentication.route("/dashboard")
@login_required
def dashboard():
	return "Welcome, " + current_user.email

@authentication.route("/signup", methods=['GET', 'POST'])
def signup():
	form = SignUpForm(request.form)
	if request.method == "POST":
		if form.validate_on_submit():
			newUser = User(form.email.data, form.password.data)
			db.session.add(newUser)
			db.session.commit()
			login_user(form.get_user())
			return redirect(url_for('.dashboard'))
		else:
			return render_template('signup.html', form=form)
	elif request.method == "GET":
		return render_template("signup.html", form=form)

@authentication.route("/login", methods=['GET', 'POST'])
def login():
	form = LoginForm(request.form)
	if not current_user.is_authenticated:
		if request.method == "POST":
			if form.validate_on_submit():
				login_user(form.get_user())
				flash('Logged in successfully.')
				next = request.args.get('next')

				#if not next_is_valid(next):
				#	return abort(400)
				return redirect(next or url_for('.dashboard'))
			#return 'logged in'
		return render_template('login.html', form=form)
	else:
		return redirect(url_for('.dashboard'))

@authentication.route("/logout")
@login_required
def logout():
	logout_user()
	return 'logout'


@authentication.route("/secret")
#@rbac.deny(['student'], ['GET'], with_children=False)
@login_required
def secret():
	return "secret"

@authentication.route("/update")
def update():
	user1 = User("h@m.com", "raju")
	role1 = Role(name='secret')
	role2 = Role(name='agent')
	user1.roles.append(role1)
	user1.roles.append(role2)
	db.session.add(user1)
	db.session.commit()
	return "success"