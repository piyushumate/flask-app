from flask import Flask, Blueprint, session, g, render_template_string, redirect, request
from flask_restful import Api, Resource, url_for
from . import github
from models import User
from .. import db
import requests, json

github_authentication = Blueprint('github_authentication', __name__)
github_host ='https://api.github.com/'
client_details = 'client_id=1f0d94ab271670fefc74&client_secret=8013d0bf98c0674aa3ee3eea808263873418056e'
# api = Api(github_authentication)

# class TodoItem(Resource):
# 	def get(self, id):
# 		return {'task': 'Say "Hello, World!"'}

# api.add_resource(TodoItem, '/todos/<int:id>')

@github.access_token_getter
def token_getter():
    user = g.user
    if user is not None:
        return user.github_access_token

@github_authentication.before_request
def before_request():
	g.user = None
	if 'user_id' in session:
		g.user = User.query.get(session['user_id'])

@github_authentication.route('/index')
def index():
	if g.user:
		t = 'Hello! <a href="{{ url_for(".user") }}">Get user</a> ' \
		'<a href="{{ url_for(".logout") }}">Logout</a>'
	else:
		t = 'Hello! <a href="{{ url_for(".login") }}">Login</a>'

	return render_template_string(t)

@github_authentication.route('/github-callback')
@github.authorized_handler
def authorized(access_token):
	next_url = request.args.get('next') or url_for('.index')
	if access_token is None:
		return redirect(next_url)

	user = User.query.filter_by(github_access_token=access_token).first()
	if user is None:
		user = User(access_token)
		db.session.add(user)
	user.github_access_token = access_token
	db.session.commit()

	session['user_id'] = user.id
	return redirect(next_url)


@github_authentication.route('/login')
def login():
	if session.get('user_id', None) is None:
		return github.authorize(scope = 'user:email,read:org,read:public_key')
	else:
		return 'Already logged in'

@github_authentication.route('/logout')
def logout():
	session.pop('user_id', None)
	return redirect(url_for('.index'))

@github_authentication.route('/user')
def user():
	return str(github.get('user'))

@github_authentication.route('/orgs')
def is_practo_organization_member():
	orgs = github.get('user/orgs')
	return 'done'

@github_authentication.route('/file_listing')
def file_listing():
	response = github.get('repos/practo/fabric/git/trees/master?recursive=1')
	print response.get('truncated')

	if 'tree' in response:
		for node in response['tree']:
			if 'path' in node:
				print node['path']
	return 'done'
	#return str(github.get('users/piyushumate/orgs'))


@github_authentication.route('/file_listing_unauthenticated')
def file_listing_unauthenticated():
	#print github_host+'repos/piyushumate/Ace-coder/git/trees/master?recursive=1&client_id=1f0d94ab271670fefc74&client_secret=8013d0bf98c0674aa3ee3eea808263873418056e'
	response_body = requests.get(github_host+'repos/piyushumate/Ace-coder/git/trees/master?recursive=1&client_id=1f0d94ab271670fefc74&client_secret=8013d0bf98c0674aa3ee3eea808263873418056e')
	response = json.loads(response_body.text)
	
	if 'tree' in response:
		for node in response['tree']:
	 		if 'path' in node:
	 			print node['path']

 	return "dude"

@github_authentication.route('/hook')
def hook():
	
	url  = github_host + 'repos/piyushumate/Ace-coder/hooks?' + client_details
	payload = { 'name': 'web', 'active': True, 'events': ['push', 'pull_request'],'config': { 'url': 'http://localhost:5000/hook_it','content_type': 'json' }}

	parameters_json = json.dumps(payload)
	headers = {'Content-Type': 'application/json'}
	response = requests.post(url, data=parameters_json, headers=headers)
	return "posted"

@github_authentication.route('/hook_it')
def hook_it():
	url = github_host + 'repos/piyushumate/Ace-coder/hooks' 
	response = github.get(url)
	print response.text
	return 'done'
