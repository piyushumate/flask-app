from .. import db 
#from sqlalchemy.ext.declarative import declarative_base

#Base = declarative_base()

class User(db.Model):

	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(200))
	github_access_token = db.Column(db.String(200))

	def __init__(self, github_access_token):
		self.github_access_token = github_access_token

