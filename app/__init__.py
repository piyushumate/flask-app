from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bcrypt import Bcrypt

# Configuring Flask application
app = Flask(__name__)
app.config.from_object('app.config')

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)


#__init__ has blueprint 
#from .authentication.controllers  import authentication as authentication_module
#app.register_blueprint(authentication_module)

#github blueprint
from .github_authentication.controllers import github_authentication as github_authentication_module
app.register_blueprint(github_authentication_module)

